<div class="phrase-wrapper">
    <article class="phrases">
        <?php if (empty($phrases)) : ?>
            <div>
                <h2 class="item title">Hoppla! Noch keine Sätze vorhanden.</h2>
            </div>
        <?php else: ?>
            <?php foreach ($phrases as $key=>$value): ?>
                <?php if ($key == 0) : ?>
                    <h1><?= htmlspecialchars($value->content);?></h1>
                <?php else: ?>
                    <div class="phrase">
                        <p class="description"><?= htmlspecialchars($value->content);?></p>
                        <div class="timestamp"><?= htmlspecialchars($value->username);?> · <?= htmlspecialchars(date("d.m.Y", $value->time));?></div>
                    </div>
                <?php endif ?>
            <?php endforeach ?>
        <?php endif ?>
    </article>
    <div class="add-anchor-container">
        <a href="#phrase-form" title="Nach unten scrollen">
            <img class="scroll-arrow" src="/images/add.svg" alt="Nach unten scrollen">
            <p>Neuer Satz <br> hinzufügen</p>
        </a>
    </div>
</div>

<div class="error-message-container danger">
		<?php foreach($errorMessages as $errorMessage) : ?>
            	<p id="bootstrap-overrides" class="error-message-text"><?= htmlspecialchars($errorMessage); ?></p>
		<?php endforeach ?>
</div>

<form id="phrase-form" action="/phrase/addPhrase" method="post">
    <?php if(isset($_SESSION["username"])) : ?>
        <textarea class="content-input" name="content" form="phrase-form" maxlength="500" placeholder="Schreibe die Geschichte weiter..."></textarea>
        <input type="submit" class="page-button">
    <?php else : ?>
        <textarea disabled class="content-input" name="content" form="phrase-form" maxlength="500" placeholder="Schreibe die Geschichte weiter..."></textarea>
        <input disabled type="submit" class="page-button">
    <?php endif ?>
</form>

<div>
    <a href="#" class="arrow-up" title="Nach oben scrollen"><img class="scroll-arrow upside-down" src="/images/arrow.svg" alt="Nach unten scrollen"></a>
</div>