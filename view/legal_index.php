<div class="header">
    <h1>Impressum</h1>
</div>
<div class="sources">
    <div class="source-category">
        <h2>Bilder</h2>
        <div class=source-list>
            <div>
                Logo gemacht von <a href="https://www.freepik.com/" title="Freepik">Freepik</a> auf
                <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
                ist lizensiert unter <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
            </div>
            <div>
                Pfeil-Icon gemacht von <a href="https://www.flaticon.com/authors/lucy-g" title="Lucy G">Lucy G</a> auf
                <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
                ist lizensiert unter <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
            </div>
            <div>
                Plus-Icon gemacht von <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> auf
                <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
                ist lizensiert unter <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>
            </div>
        </div>
    </div>
    <div class="source-category">
        <h2>Fonts</h2>
        <div class="source-list">
            <p>https://fonts.google.com/specimen/Roboto</p>
            <p>https://fonts.google.com/specimen/Lora</p>
        </div>
    </div>
</div>