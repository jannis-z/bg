<div class="login-container">
	<div class="login-brand">
		<div>
			<img src="/images/open-book.svg" class="logo" alt="Nach unten scrollen">
		</div>
		<div>
			<h2>Bestseller Generator</h2>
		</div>
    </div>

    <div class="error-message-container danger">
		<?php foreach($errorMessages as $errorMessage) : ?>
            	<p id="bootstrap-overrides" class="error-message-text"><?= htmlspecialchars($errorMessage); ?></p>
		<?php endforeach ?>
	</div>
	
	<form class="login-form" action="/user/doCreate" method="post">
		<div class="input-field">
			<label for="username">Benutzername</label>
			<input name="username"
			id="username" type="text"
			maxlength="30"
			onkeypress="return avoidSpace(event)"
			placeholder="Benutzername">
		</div>
		<div class="input-field">
			<label for="dateOfBirth">Geburtsdatum</label>
			<input
				name="dateOfBirth"
				id="dateOfBirth"
				type="date"
				value="<?= htmlspecialchars(date("Y-m-d"));?>"
				min="1900-01-01"
				max="<?= htmlspecialchars(date("Y-m-d"));?>">
		</div>
		<div class="input-field">
			<label for="password">Passwort</label>
			<input name="password"
			id="password"
			type="password"
			maxlength="60"
			onkeypress="return avoidSpace(event)"
			placeholder="Passwort">
		</div>
		<input name="send" type="submit" value="Registrieren" class="login-button page-button">
	</form>
</div>
