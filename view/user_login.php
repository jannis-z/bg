<div class="login-container">
    <div class="login-brand">
        <div>
            <img src="/images/open-book.svg" class="logo" alt="Nach unten scrollen">
        </div>
        <div>
            <h2>Bestseller Generator</h2>
        </div>
    </div>
    
    <div class="error-message-container danger">
		<?php foreach($errorMessages as $errorMessage) : ?>
            	<p id="bootstrap-overrides" class="error-message-text"><?= htmlspecialchars($errorMessage); ?></p>
		<?php endforeach ?>
	</div>

    <form action="/user/doLogin" method="post" class="login-form">
        <div class="input-field">
            <label for="username">Benutzername</label>
            <input type="text" name="username" id="username" placeholder="Benutzername"><br>
        </div>
        <div class="input-field">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" placeholder="Passwort"><br>
        </div>
        <input type="submit" name="send" class="login-button page-button" value="Anmelden">
    </form>
    <a href="/user/create" title="Registrieren">Registrieren</a>
</div>
