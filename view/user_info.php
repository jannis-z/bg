<div class="header">
    <h1>Mein Konto</h1>
</div>

<div class="error-message-container danger">
	<?php foreach($errorMessages as $errorMessage) : ?>
			<p id="bootstrap-overrides" class="error-message-text"><?= htmlspecialchars($errorMessage); ?></p>
	<?php endforeach ?>
</div>

<div class="info-container">
    <form class="update-form" action="/user/update" method="post">
		<div class="input-field">
			<label for="firstName">Benutzername: </label>
			<input name="username" type="text" maxlength="30" value="<?= htmlspecialchars($user->username); ?>" onkeypress="return avoidSpace(event)" placeholder="Benutzername">
		</div>
		<div class="input-field">
			<label for="email">Geburtsdatum: </label>
			<input 
				name="dateOfBirth"
				type="date"
				value="<?= htmlspecialchars($user->date_of_birth); ?>"
				min="1900-01-01"
				max=<?= htmlspecialchars(date("Y-m-d"));?>>
		</div>
		<div class="input-field">
			<label for="password">Neues Passwort: </label>
			<input name="password" type="password" maxlength="60" onkeypress="return avoidSpace(event)" placeholder="Passwort">
        </div>
        <div class="crud-buttons">
            <input name="send" type="submit" value="Speichern" class="page-button">
        </div>	
    </form>
	<form action="/user/delete">
    	<div class="crud-buttons delete">
			<input type="submit" class="page-button danger" value="Konto löschen">
		</div>
	</form>
</div>