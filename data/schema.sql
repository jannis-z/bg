﻿DROP TABLE IF EXISTS phrase_story;
DROP TABLE IF EXISTS story;
DROP TABLE IF EXISTS phrase;
DROP TABLE IF EXISTS user;

CREATE TABLE story (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  title VARCHAR(45) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE `user` (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  username VARCHAR(30) UNIQUE,
  date_of_birth date,
  password char(60) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE phrase (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  content VARCHAR(500) NOT NULL,
  time INT(11),
  user_id INT UNSIGNED,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user (id)
  ON DELETE SET NULL ON UPDATE CASCADE
);

CREATE TABLE phrase_story (
  phrase_id INT UNSIGNED NOT NULL,
  story_id INT UNSIGNED NOT NULL,
  FOREIGN KEY (phrase_id) REFERENCES phrase (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (story_id) REFERENCES story (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (phrase_id, story_id)
);

INSERT INTO story (id, title) VALUES (1, "Die erste Geschichte");

INSERT INTO user (id, username, password, date_of_birth) VALUES (1, "THEstoryTELLER", "unhashedPW", CURDATE());
INSERT INTO user (id, username, password, date_of_birth) VALUES (2, "fishHunter22", "unhashedPW", CURDATE());
INSERT INTO user (id, username, password, date_of_birth) VALUES (3, "HorstSchmied", "unhashedPW", CURDATE());
INSERT INTO user (id, username, password, date_of_birth) VALUES (4, "pinkpanther47", "unhashedPW", CURDATE());
INSERT INTO user (id, username, password, date_of_birth) VALUES (5, "legend27", "unhashedPW", CURDATE());

INSERT INTO phrase (id, content) VALUES (1, "Hell scheint der Mond, als die Katze beginnt zu heulen.");
INSERT INTO phrase (id, content, time, user_id) VALUES
(2, "Die ganze Welt hält den Atem still.", -52531200, 1),
(3, "Nur er wirkt tiefst entspannt. Spielerisch lässt er ein altes Taschenmesser auf und zu klappen.", -22531200, 2),
(4, "Lorem ipsum.", -12501200, 3),
(5, "Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.", 698203, 4),
(6, "Sader quin dolore alaiumu.", 100628203, 5),
(7, "Labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.", 300698213, 4),
(8, "Da diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.", 450696213, 3),
(9, "At vero eos et accusam et justo duo dolores et ea rebum.", 900691213, 1),
(10, "Erat, sed diam voluptua. At vero eos et accusam et justo duo. Labore et dolore magna aliquyam erat, sed diam voluptua. Erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.", 1000681213, 5),
(11, "Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna", 1010681213, 1),
(12, "Eos et accusam et justo duo dolores et ea rebum.", UNIX_TIMESTAMP(), 2);

INSERT INTO phrase_story (phrase_id, story_id) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1);