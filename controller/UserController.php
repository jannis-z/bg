<?php

require_once "../repository/UserRepository.php";

/**
 * Siehe Dokumentation im DefaultController.
 */
class UserController
{
    public function index()
    {
        $this->login();
    }

    public function login($response = [])
    {
        $view = new View("user_login");
        $view->title = "Anmelden";

        // Create error messages
        $errorMessages = [];
        foreach($response as $key=>$value) {
            if($key == "usernameAndPasswordAreCorrect" && !$value) {
                array_push($errorMessages, "Benutzername oder Passwort ist nicht korrekt.");
            }
        }

        $view->errorMessages = $errorMessages;
        $view->display();
    }

    public function doLogin()
    {
        // If $_POST["send"] is set and method was triggered by a POST request
        if (isset($_POST["send"]) && $_POST["send"]) {
            $username = $_POST["username"];
            $password = $_POST["password"];
            
            $userRepository = new UserRepository();

            // checkUsernameAndPassword returns a response with errors
            $response = $userRepository->checkUsernameAndPassword($username, $password);
            if(!in_array(false, $response)) { // If there is no error
                $_SESSION["username"] = $username; // Save username in session
                header("location: /phrase");
            } else {
                $this->login($response); // Go back to login
            }
        } else {
            header("location: /user/login");
        }
    }

    public function logout()
    {
        unset($_SESSION["username"]);
        session_destroy();
        header("location: /user/login");
    }

    public function create($response = [])
    {
        $view = new View("user_create");
        $view->title = "Registrieren";
        
        // Let error messages be created
        $errorMessages = $this->getErrorMessagesForUsernamePasswordDate($response);
        $view->errorMessages = $errorMessages;

        $view->display();
    }
    
    public function getErrorMessagesForUsernamePasswordDate($response = NULL)
    {
        if($response !== NULL) {
            $errorMessages = [];
            foreach($response as $key=>$value) {
                switch($key) {
                    case "usernameLengthIsOkay" :
                        if(!$value) {
                            array_push($errorMessages, "Der Benutzername muss aus mindestens 4 und aus höchstens 30 Zeichen bestehen.");
                        }
                        break;
                    case "passwordLengthIsOkay" :
                        if(!$value) {
                            array_push($errorMessages, "Das Passwort muss aus mindestens 8 und höchstens 80 Zeichen bestehen.");
                        }
                        break;
                    case "usernameNotExisting" :
                        if(!$value) {
                            array_push($errorMessages, "Dieser Benutzername existiert bereits.");
                        }
                        break;
                    case "usernameNotEqualToPassword" :
                        if(!$value) {
                            array_push($errorMessages, "Das Passwort darf nicht dem Benutzernamen entsprechen.");
                        }
                        break;
                    case "dateIsOkay" :
                        if(!$value) {
                            array_push($errorMessages, "Ungültiges Datum");
                        }
                        break;
                    case "usernameCharactersAreValid" :
                        if(!$value) {
                            array_push($errorMessages, "Der Benutzername enthält ungültige Zeichen.");
                        }
                        break;
                    case "passwordCharactersAreValid" :
                        if(!$value) {
                            array_push($errorMessages, "Das Passwort enthält ungültige Zeichen.");
                        }
                        break;
                }
            }
            return $errorMessages;
        } else {
            header("location: /default");
        }
    }

    public function doCreate()
    {
        // If $_POST["send"] is set and method was triggered by a POST request
        if (isset($_POST["send"]) && $_POST["send"]) {
            $username = $_POST["username"];
            $dateOfBirth = $_POST["dateOfBirth"];
            $password = $_POST["password"];
            
            $userRepository = new UserRepository();

            // validateUserInput returns a response with errors
            $response = $this->validateUserInput($username, $dateOfBirth, $password);

            if(!in_array(false, $response)) { // If there is no error
                $userRepository->create($username, $dateOfBirth, $password);
                $this->doLogin(); // Log in the user after creating
            } else {
                $this->create($response); // Go back to create
            }
        } else {
            header("location: /user/create");
        }
    }
    
    public function validateUserInput($username = NULL, $dateOfBirth = NULL, $password = NULL)
    {
        if($username !== NULL && $dateOfBirth !== NULL && $password !== NULL) {
            $userRepository = new UserRepository();
    
            $response["usernameCharactersAreValid"] = !preg_match("/\s/", $username);
            $response["passwordCharactersAreValid"] = !preg_match("/\s/", $username);
            $response["usernameLengthIsOkay"] = strlen($username) >= 4 && strlen($username) <= 30;
            $response["passwordLengthIsOkay"] = strlen($password) >= 8 && strlen($password) <= 80;
            $response["usernameNotExisting"] = ($userRepository->getUser($username) == NULL);
            $response["usernameNotEqualToPassword"] = ($username != $password);
            $response["dateIsOkay"] = $this->validateDate($dateOfBirth);
    
            return  $response;
        } else {
            header("location: /default");
            return;
        }
    }

    // Helper function from: http://php.net/manual/de/function.checkdate.php#113205
    public function validateDate($date, $format = "Y-m-d")
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    public function info($response = [])
    {
        if(isset($_SESSION["username"])) { // If user is logged in
            $userRepository = new UserRepository();
    
            $view = new View("user_info");
            $view->title = $_SESSION["username"];
            $view->user = $userRepository->getUser($_SESSION["username"]);
            
            // Let error messages be created
            $errorMessages = $this->getErrorMessagesForUsernamePasswordDate($response);
            $view->errorMessages = $errorMessages;
            
            $view->display();
        } else {
            header("location: /user/login"); // Go to login page
        }
    }

    public function update()
    {
        // If user is logged in and if method was triggered by a POST request
        if(isset($_SESSION["username"]) && $_POST["send"]) {
            $username = $_POST["username"];
            $dateOfBirth = $_POST["dateOfBirth"];
            $password = $_POST["password"];

            $userRepository = new UserRepository();

            // validateUserInput returns a response with errors
            $response = $this->validateUserInput($username, $dateOfBirth, $password);

            // If username has not changed usernameNotExisting should be true
            if($_SESSION["username"] == $username) {
                $response["usernameNotExisting"] = true;
            }

            if($password == "") {
                $password = NULL;
                $response["passwordLengthIsOkay"] = true;
            }

            if(!in_array(false, $response)) { // If there are no errors
                $userRepository->update($username, $dateOfBirth, $password);
                $_SESSION["username"] = $username; // Reset the $_SESSION["username"] variable
                header("location: /user/info");
            } else {
                $this->info($response); // Go to info
            }
        } else {
            header("location: /user/login");
        }
    }

    public function delete()
    {
        // If user is logged in
        if(isset($_SESSION["username"])) {
            $userRepository = new UserRepository();
            $userRepository->deleteUser($_SESSION["username"]);

            // Unset and destroy session
            unset($_SESSION["username"]);
            session_destroy();

            header("location: /phrase");
        }
    }
    
}