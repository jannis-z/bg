<?php

/**
 * Siehe Dokumentation im DefaultController.
 */
class LegalController
{
    public function index()
    {
        $view = new View("legal_index");
        $view->title = "Impressum";
        $view->display();
    }
}
