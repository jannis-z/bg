<?php

require_once "../repository/PhraseRepository.php";

/**
 * Siehe Dokumentation im DefaultController.
 */
class PhraseController
{
    public function index($response = [])
    {
        $phraseRepository = new PhraseRepository();

        $view = new View("phrase_index");
        $view->title = "Home";
        $view->phrases = $phraseRepository->getPhrases();

        $response["isLoggedIn"] = isset($_SESSION["username"]);

        $errorMessages = [];
        foreach($response as $key=>$value) {
            switch($key) {
                case "isLoggedIn" :
                    if(!$value) {
                        array_push($errorMessages, "Du musst dich anmelden um die Geschichte zu erweitern.");
                    }
                    break;
                case "lengthIsOkay" :
                    if(!$value) {
                        array_push($errorMessages, "Der Satz darf aus 5 bis 500 Zeichen bestehen.");
                    }
                    break;
                case "24hPastSinceLastPhrase" :
                    if(!$value) {
                        array_push($errorMessages, "Du darfst die Geschichte nur alle 24 Stunden erweitern.");
                    }
                    break;
            }
        }

        $view->errorMessages = $errorMessages;
        $view->display();
    }

    public function addPhrase()
    {
        // If $_POST["content"] is set
        if(isset($_POST["content"])) {
        
            // checkPhrase returns a response with errors
            $response = $this->checkPhrase($_POST["content"]);
            if(!in_array(false, $response)) { // If there are no errors
                $phraseRepository = new PhraseRepository();
                $phraseRepository->addPhrase($_POST["content"]);
                header("location: /phrase");
            } else {
                $this->index($response);
            }
        } else {
            header("location: /phrase");
        }
    }

    public function checkPhrase($content = NULL)
    {
        // Method can not be called directly from the URI
        if($content !== NULL) {
            $phraseRepository = new PhraseRepository();
    
            $response["isLoggedIn"] = isset($_SESSION["username"]);
            $response["lengthIsOkay"] = strlen($content) >= 5 && strlen($content) <= 500;
    
            if(isset($_SESSION["username"])) {
                $latestPhrase = $phraseRepository->getLatestPhraseOfUser($_SESSION["username"]);
                $response["24hPastSinceLastPhrase"] = ($latestPhrase == NULL || time() >= $latestPhrase->time + 60 * 60 * 24);
            } else {
                $response["24hPastSinceLastPhrase"] = true;
            }
            return $response;
        } else {
            header("location: /phrase");
        }
    }

}
