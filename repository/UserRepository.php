<?php

require_once "../lib/Repository.php";

/**
 * Das UserRepository ist zuständig für alle Zugriffe auf die Tabelle "user".
 *
 * Die Ausführliche Dokumentation zu Repositories findest du in der Repository Klasse.
 */
class UserRepository extends Repository
{
    /**
     * Diese Variable wird von der Klasse Repository verwendet, um generische
     * Funktionen zur Verfügung zu stellen.
     */
    protected $tableName = "user";

    /**
     * Erstellt einen neuen benutzer mit den gegebenen Werten.
     *
     * Das Passwort wird vor dem ausführen des Queries noch mit dem SHA1
     *  Algorythmus gehashed.
     *
     * @throws Exception falls das Ausführen des Statements fehlschlägt
     */
    public function create($username, $dateOfBirth, $password)
    {
        // Adds cost to the password hash
        $options = [
            "cost" => 10,
        ];

        // Hash password with bcrypt and salt
        $hashed_password = password_hash($password, PASSWORD_BCRYPT, $options);

        $query = "INSERT INTO $this->tableName (username, date_of_birth, password) VALUES (?, ?, ?)";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param("sss", $username, $dateOfBirth, $hashed_password);

        if (!$statement->execute()) {
            throw new Exception($statement->error);
        }
    }

    public function checkUsernameAndPassword($username, $password)
    {
        $query = "SELECT username, password FROM user WHERE username = ? LIMIT 1";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param("s", $username);

        if (!$statement->execute()) {
            throw new Exception($statement->error);
        }

        $user = $statement->get_result()->fetch_object();
        
        $response["usernameAndPasswordAreCorrect"] = $user && password_verify($password, $user->password);

        return  $response;
    }

    public function update($username, $dateOfBirth, $password)
    {
        // Does not update password if $password is NULL
        if ($password == NULL) {
            $query = "UPDATE $this->tableName SET username = ?, date_of_birth = ? WHERE username = ?";

            $statement = ConnectionHandler::getConnection()->prepare($query);
            $statement->bind_param("sss", $username, $dateOfBirth, $_SESSION["username"]);

            if (!$statement->execute()) {
                throw new Exception($statement->error);
            }
        } else {
            // Adds cost to the password hash
            $options = [
                "cost" => 10,
            ];

            // Hash password with bcrypt and salt
            $hashed_password = password_hash($password, PASSWORD_BCRYPT, $options);

            $query = "UPDATE $this->tableName SET username = ?, date_of_birth = ?, password = ? WHERE username = ?";

            $statement = ConnectionHandler::getConnection()->prepare($query);
            $statement->bind_param("ssss", $username, $dateOfBirth, $hashed_password, $_SESSION["username"]);

            if (!$statement->execute()) {
                throw new Exception($statement->error);
            }
        }
    }

    public function getUser($username)
    {
        $query = "SELECT id, username, date_of_birth FROM user WHERE username = ? LIMIT 1";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param("s", $username);

        if (!$statement->execute()) {
            throw new Exception($statement->error);
        }

        return $statement->get_result()->fetch_object();
    }

    public function deleteUser($username)
    {
        $query = "DELETE FROM user WHERE username = ?";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param("s", $username);

        if (!$statement->execute()) {
            throw new Exception($statement->error);
        }
    }
    
}
