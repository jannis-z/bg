<?php

require_once "../lib/Repository.php";

/**
 * Das PhraseRepository ist zuständig für alle Zugriffe auf die Tabelle "phrase".
 *
 * Die Ausführliche Dokumentation zu Repositories findest du in der Repository Klasse.
 */
class PhraseRepository extends Repository
{
    /**
     * Diese Variable wird von der Klasse Repository verwendet, um generische
     * Funktionen zur Verfügung zu stellen.
     */
    protected $tableName = "phrase";

    public function getPhrases($max = 100)
    {
        $query = "SELECT phrase.content, phrase.time, user.username FROM phrase LEFT JOIN user ON phrase.user_id = user.id LIMIT 0, $max";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->execute();

        $result = $statement->get_result();
        if (!$result) {
            throw new Exception($statement->error);
        }

        $rows = array();
        while ($row = $result->fetch_object()) {
            $rows[] = $row;
        }

        return $rows;
    }
    
    public function getLatestPhraseOfUser($username)
    {
        $query = "SELECT * FROM phrase JOIN user ON phrase.user_id = user.id WHERE user.username = ? ORDER BY time DESC LIMIT 1";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param("s", $username);

        if (!$statement->execute()) {
            throw new Exception($statement->error);
        }

        return $statement->get_result()->fetch_object();
    }

    public function addPhrase($content)
    {

        $query = "INSERT INTO $this->tableName (content, time, user_id) VALUES (?, UNIX_TIMESTAMP(), (SELECT id FROM user WHERE username = ?))";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param("ss", $content, $_SESSION["username"]);

        if (!$statement->execute()) {
            throw new Exception($statement->error);
        }

        $this->addToStory($statement->insert_id);

        return $statement->insert_id;
    }

    // Adds an entry in the junction table phrase_story
    public function addToStory($phraseId)
    {
        $query = "INSERT INTO phrase_story (phrase_id, story_id) VALUES (?, 1)";

        $statement = ConnectionHandler::getConnection()->prepare($query);
        $statement->bind_param("i", $phraseId);

        if (!$statement->execute()) {
            throw new Exception($statement->error);
        }
    }
}
